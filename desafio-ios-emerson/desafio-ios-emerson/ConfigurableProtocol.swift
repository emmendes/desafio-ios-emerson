//
//  ConfigurableProtocol.swift
//  desafio-ios-emerson
//
//  Created by Emerson Mendes Filho on 03/10/16.
//  Copyright © 2016 Emerson Mendes Filho. All rights reserved.
//

import Foundation

protocol CellConfigurable{
    
    func configureCellWith(viewModelItem: Any)
}
