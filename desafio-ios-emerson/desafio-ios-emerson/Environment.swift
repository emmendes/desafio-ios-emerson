//
//  Environment.swift
//  desafio-ios-emerson
//
//  Created by Emerson Mendes Filho on 03/10/16.
//  Copyright © 2016 Emerson Mendes Filho. All rights reserved.
//

import Foundation

/**
 *  A `struct` used to retrieve the environment settings.
 */
struct Environment {
    private static var environment: [String: AnyObject]? {
        return Bundle.main.infoDictionary?["Environment settings"] as? [String: AnyObject]
    }
    
    /// The base URL for the API used for the specific environment (Production, Development and Staging)
    static var baseURL: String? {
        guard let environment = environment else {
            fatalError("Check the `Environment settings` key in the plist.")
        }
        
        let baseURL = environment["BASE_URL"] as? String
        
        return baseURL
    }
}
