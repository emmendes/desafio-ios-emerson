//
//  ErrorTypes.swift
//  desafio-ios-emerson
//
//  Created by Emerson Mendes Filho on 03/10/16.
//  Copyright © 2016 Emerson Mendes Filho. All rights reserved.
//

import Alamofire

enum ErrorTypes: Error {
    case parseError (error: Error)
    case network (error: Error)
    case jsonSerialization (error: Error)
    case objectSerialization (message: String)
}
