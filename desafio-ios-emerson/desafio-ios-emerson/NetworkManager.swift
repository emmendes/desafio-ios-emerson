//
//  NetworkManager.swift
//  desafio-ios-emerson
//
//  Created by Emerson Mendes Filho on 02/10/16.
//  Copyright © 2016 Emerson Mendes Filho. All rights reserved.
//

import Alamofire
import ObjectMapper

typealias Failure = (_ error: String) -> ()
typealias Success = (_ response: Any) -> ()

class NetworkManager {
    
    private static let sessionManager: SessionManager = {
        let configuration = URLSessionConfiguration.default
        configuration.httpAdditionalHeaders = SessionManager.defaultHTTPHeaders
        
        return SessionManager(configuration: configuration)
    }()
    
    static func request(_ URLRequest: URLRequestConvertible, success: @escaping Success, failure: @escaping Failure) {
        
        sessionManager.request(URLRequest).validate().responseJSONChecked { (response) -> Void in
            
            switch response.result {
                
            case let .success(data):
                success(data)
                break;
                
            case let .failure(error):
                failure(error.localizedDescription)
                break;
            }
        }
    }
}

extension DataRequest {
    
    @discardableResult
    func responseJSONChecked(
        queue: DispatchQueue? = nil,
        completionHandler: @escaping (DataResponse<Any>) -> Void)
        -> Self
    {
        let responseSerializer = DataResponseSerializer<Any> { request, response, data, error in
            guard error == nil else { return .failure(ErrorTypes.network(error: error!)) }
            
            let jsonResponseSerializer = DataRequest.jsonResponseSerializer(options: .allowFragments)
            let result = jsonResponseSerializer.serializeResponse(request, response, data, nil)
            
            guard case let .success(jsonObject) = result else {
                return .failure(ErrorTypes.jsonSerialization(error: result.error!))
            }
            
            return .success(jsonObject)
        }
        
        return response(queue: queue, responseSerializer: responseSerializer, completionHandler: completionHandler)
    }
}
