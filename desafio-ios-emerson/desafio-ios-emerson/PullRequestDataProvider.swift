//
//  PullRequestDataProvider.swift
//  desafio-ios-emerson
//
//  Created by Emerson Mendes Filho on 03/10/16.
//  Copyright © 2016 Emerson Mendes Filho. All rights reserved.
//

import Foundation
import UIKit

protocol PullRequestDataProviderDelegate: class {
    
    func selectedPullRequest(pullRequest: PullRequestModel)
}

class PullRequestDataProvider: NSObject, UITableViewDelegate, UITableViewDataSource {
    
    var pullRequestViewModel: PullRequestViewModel?
    weak var delegate: PullRequestDataProviderDelegate?
    private var shouldHideTopViewCompletion: ((_ hide: Bool) -> ())?
    
    override init() {
        super.init()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let repo = pullRequestViewModel else  {
            return 0
        }
        
        return repo.pullRequestList!.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let viewModelItem = pullRequestViewModel!.itemForIndex(index: indexPath.row)
        
        let cell: PullRequestTableViewCell = tableView.dequeueReusableCell(withIdentifier: String(describing: PullRequestTableViewCell.self), for: indexPath) as! PullRequestTableViewCell
        
        cell.configureCellWith(viewModelItem: viewModelItem)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
        delegate!.selectedPullRequest(pullRequest: (self.pullRequestViewModel!.pullRequestForIndex(index: indexPath.row)))
    }
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        if scrollView.panGestureRecognizer.translation(in: scrollView).y < 0{
            print("drag up")
            shouldHideTopViewCompletion!(true)
        } else {
            print("drag down")
            shouldHideTopViewCompletion!(false)
        }
    }
    
    func addShouldHideTopViewCompletion(completion: @escaping (_ hide: Bool)->()){
        self.shouldHideTopViewCompletion = completion
    }
}
