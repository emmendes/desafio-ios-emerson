//
//  PullRequestModel.swift
//  desafio-ios-emerson
//
//  Created by Emerson Mendes Filho on 02/10/16.
//  Copyright © 2016 Emerson Mendes Filho. All rights reserved.
//

import ObjectMapper

class PullRequestModel: Mappable {
    
    
    enum PullRequestState : String {
        case open = "open"
        case close = "closed"
    }
    
    //
    //MARK: Var Declaration
    //
    
    var title: String?
    var body: String?
    var user: UserModel?
    var createdAt: Date?
    var htmlUrl: URL?
    var state: PullRequestState?
    
    //
    //MARK: Init Methods
    //
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        title <- map["title"]
        body <- map["body"]
        user <- map["user"]
        createdAt <- (map["created_at"], DateTransform())
        htmlUrl <- (map["html_url"], URLTransform())
        state <- (map["state"], EnumTransform<PullRequestState>())
    }
}

extension PullRequestModel {
    
    static func loadPullRequests(creator: String, repository: String, success: @escaping (_ list: [PullRequestModel]) -> (), failure: @escaping Failure) {
        
        NetworkManager.request(Router.pullRequests(creator: creator, repo: repository), success: { (response) in
            
            guard let list = Mapper<PullRequestModel>().mapArray(JSONObject: response) else {
                failure("Could Not Parse Data \(response)")
                return
            }   
            success(list)
            
        }) { (error) in
            failure(error)
        }
    }
}
