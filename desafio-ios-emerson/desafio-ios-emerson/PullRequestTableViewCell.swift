//
//  PullRequestTableViewCell.swift
//  desafio-ios-emerson
//
//  Created by Emerson Mendes Filho on 03/10/16.
//  Copyright © 2016 Emerson Mendes Filho. All rights reserved.
//

import UIKit
import Kingfisher

class PullRequestTableViewCell: UITableViewCell, CellConfigurable {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var bodyLabel: UILabel!
    @IBOutlet weak var ownerAvatarImageView: UIImageView!
    @IBOutlet weak var ownerLoginLabel: UILabel!
    @IBOutlet weak var ownerNameLabel: UILabel!
    
    func configureCellWith(viewModelItem: Any) {
        
        let viewModelItem = viewModelItem as! PullRequestViewModel.ViewModelItem
        
        self.titleLabel.text = viewModelItem.title
        self.bodyLabel.text = viewModelItem.body
        self.ownerNameLabel.text = "Username"
        self.ownerLoginLabel.text = viewModelItem.userLogin
        self.ownerAvatarImageView.kf.setImage(with: viewModelItem.userAvatarUrl,
                                              placeholder: Image(named: "no_photo_picture"),
                                              options: [.transition(.fade(1))],
                                              progressBlock: nil,
                                              completionHandler: nil)
        
        
    }
    
}
