//
//  PullRequestViewController.swift
//  desafio-ios-emerson
//
//  Created by Emerson Mendes Filho on 03/10/16.
//  Copyright © 2016 Emerson Mendes Filho. All rights reserved.
//

import UIKit

class PullRequestViewController: UIViewController, PullRequestDataProviderDelegate {
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var pullRequestStatesCountLabel: UILabel!
    @IBOutlet weak var pullRequestViewStatesViewHeightConstraint: NSLayoutConstraint!
    
    var repository: RepositoryModel?
    var pullRequestViewModel = PullRequestViewModel()
    var refreshControl: UIRefreshControl!
    var pullRequestDataProvider = PullRequestDataProvider()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        startLoading()
        configureViews()
        refresh(sender: self)
    }
    
    func configureViews() {
        
        self.title = self.repository?.name!
        self.tableView.register(UINib(nibName: String(describing: PullRequestTableViewCell.self), bundle: nil),
                                forCellReuseIdentifier: String(describing: PullRequestTableViewCell.self))
        self.tableView.delegate = self.pullRequestDataProvider
        self.tableView.dataSource = self.pullRequestDataProvider
        self.tableView.rowHeight = UITableViewAutomaticDimension
        self.tableView.estimatedRowHeight = 113
        self.tableView.tableFooterView = UIView()
        
        refreshControl = UIRefreshControl()
        refreshControl.attributedTitle = NSAttributedString(string: "Refresh")
        refreshControl.addTarget(self, action: #selector(refresh(sender:)), for: UIControlEvents.valueChanged)
        tableView.addSubview(refreshControl)
        
        self.pullRequestDataProvider.addShouldHideTopViewCompletion { (hide) in
            if (hide) {
                self.pullRequestViewStatesViewHeightConstraint.constant = 0
            } else {
                self.pullRequestViewStatesViewHeightConstraint.constant = 25
            }
            
            UIView.animate(withDuration: 0.2, animations: {
                self.view.layoutIfNeeded()
            })
        }
    }
    
    func refresh(sender: AnyObject) {
        
        self.pullRequestViewModel.loadPullRequests(creator: (repository!.owner?.login!)!, repository: repository!.name!, completion: {
            self.stopLoading()
            self.pullRequestDataProvider.pullRequestViewModel = self.pullRequestViewModel
            self.pullRequestStatesCountLabel.attributedText = self.pullRequestViewModel.pullRequestStatesCountFormatted
            self.pullRequestDataProvider.delegate = self
            self.tableView.reloadData()
        }) { (error) in
            self.stopLoading()
            print(error)
        }
    }
    
    func startLoading() {
        self.activityIndicator.startAnimating()
        self.tableView.isHidden = true
    }
    
    func stopLoading() {
        self.refreshControl.endRefreshing()
        self.activityIndicator.stopAnimating()
        self.tableView.isHidden = false
    }
    
    func selectedPullRequest(pullRequest: PullRequestModel) {
        UIApplication.shared.open(pullRequest.htmlUrl!, options: [:], completionHandler: nil)
    }
    
}
