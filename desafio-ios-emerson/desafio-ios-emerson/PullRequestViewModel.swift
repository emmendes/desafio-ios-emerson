//
//  PullRequestViewModel.swift
//  desafio-ios-emerson
//
//  Created by Emerson Mendes Filho on 03/10/16.
//  Copyright © 2016 Emerson Mendes Filho. All rights reserved.
//

import Foundation
import UIKit

class PullRequestViewModel {
    
    var pullRequestList: [PullRequestModel]?
    
    lazy var pullRequestStatesCountFormatted: NSAttributedString = {
        return self.formatPullRequestStates()
    }()
    
    struct ViewModelItem {
        var title: String
        var body: String
        var userLogin: String
        var userAvatarUrl: URL
    }
    
    func loadPullRequests(creator: String, repository: String, completion:@escaping () -> (), failure: @escaping Failure) {
        
        PullRequestModel.loadPullRequests(creator: creator, repository: repository, success: {[weak self] (pullRequestList) in
            guard let strongSelf = self else { return }
            strongSelf.pullRequestList = pullRequestList
            completion()
            
            }) { (errorString) in
                failure(errorString)
        }
    }
    
    func pullRequestForIndex(index: Int) -> PullRequestModel {
        let pullRequestModel = self.pullRequestList![index]
        return pullRequestModel
    }
    
    func itemForIndex(index: Int) -> ViewModelItem {
        
        let pullRequestModel = self.pullRequestList![index]
        
        let viewModelItem = ViewModelItem(title: pullRequestModel.title ?? "",
                                          body: pullRequestModel.body ?? "",
                                          userLogin: pullRequestModel.user!.login ?? "",
                                          userAvatarUrl: (pullRequestModel.user?.avatarUrl)! as URL)
        return viewModelItem
        
    }
    
    func formatPullRequestStates() -> NSAttributedString {
        let openStateCount = self.pullRequestList!.filter({$0.state == .open}).count
        let closeStateCount = self.pullRequestList!.filter({$0.state == .close}).count
        
        
        let openedString = NSAttributedString(string: "Opened \(openStateCount)", attributes: [NSForegroundColorAttributeName: UIColor.orange,
                                                                                               NSFontAttributeName: UIFont.boldSystemFont(ofSize: 12)])
        let closedString = NSAttributedString(string: " \\ Closed \(closeStateCount)", attributes: [NSForegroundColorAttributeName: UIColor.black,
                                                                                                    NSFontAttributeName: UIFont.boldSystemFont(ofSize: 12)])
        
        let myString = NSMutableAttributedString()
        myString.append(openedString)
        myString.append(closedString)
        
        return myString
    }
}
