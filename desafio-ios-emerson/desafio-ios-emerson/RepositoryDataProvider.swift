//
//  RepositoryDataProvider.swift
//  desafio-ios-emerson
//
//  Created by Emerson Mendes Filho on 03/10/16.
//  Copyright © 2016 Emerson Mendes Filho. All rights reserved.
//

import UIKit

protocol RepositoryDataProviderDelegate: class {
    func selectedRepository(repository: RepositoryModel)
}

class RepositoryDataProvider: NSObject, UITableViewDelegate, UITableViewDataSource {
    
    var repositoryViewModel: RepositoryViewModel?
    var isLoadingNextPage = false
    private var nextPageCompletion: (() -> ())?
    weak var delegate: RepositoryDataProviderDelegate?
    
    override init() {
        super.init()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let repo = repositoryViewModel else  {
            return 0
        }
        
        return repo.repositoryList!.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let viewModelItem = repositoryViewModel!.itemForIndex(index: indexPath.row)
        let cell: RepositoryTableViewCell = tableView.dequeueReusableCell(withIdentifier: String(describing: RepositoryTableViewCell.self)) as! RepositoryTableViewCell
        cell.configureCellWith(viewModelItem: viewModelItem)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
        delegate!.selectedRepository(repository: (self.repositoryViewModel!.repositoryForIndex(index: indexPath.row)))
    }
    
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        if let completion = nextPageCompletion {
            
            let offset = scrollView.contentOffset.y
            let maxOffset = scrollView.contentSize.height - scrollView.frame.size.height
            if (maxOffset - offset) <= 10 {
                if (!isLoadingNextPage) {
                    isLoadingNextPage = true
                    print("bottom")
                    completion()
                }
            }
        }
    }
    
    func addInfiniteScrollCompletion(completion: @escaping ()->()){
        self.nextPageCompletion = completion
    }
}
