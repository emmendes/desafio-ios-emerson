//
//  RepositoryModel.swift
//  desafio-ios-emerson
//
//  Created by Emerson Mendes Filho on 02/10/16.
//  Copyright © 2016 Emerson Mendes Filho. All rights reserved.
//

import ObjectMapper

class RepositoryModel: Mappable {
    
    //
    //MARK: Var Declaration
    //
    
    var name: String?
    var description: String?
    var owner: UserModel?
    var score: Int?
    var forksCount: Int?
    
    //
    //MARK: Init Methods
    //
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        name <- map["name"]
        description <- map["description"]
        owner <- map["owner"]
        forksCount <- map["forks_count"]
        score <- map["score"]
    }
}

extension RepositoryModel {
    
    static func loadRepositoryList(page: Int, success: @escaping (_ list: [RepositoryModel]) -> (), failure: @escaping Failure) {
        
        NetworkManager.request(Router.repositories(page: page), success: { (response) in
            
            print(String(page))
            let responseDict = response as? [String: Any]
            guard let list = Mapper<RepositoryModel>().mapArray(JSONObject: responseDict?["items"]) else {
                failure("Could Not Parse Data \(response)")
                return
            }
            success(list)
            
        }) { (error) in
            failure(error)
        }
    }
    
}
