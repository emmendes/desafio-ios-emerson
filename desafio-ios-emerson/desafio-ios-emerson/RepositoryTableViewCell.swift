//
//  RepositoryTableViewCell.swift
//  desafio-ios-emerson
//
//  Created by Emerson Mendes Filho on 03/10/16.
//  Copyright © 2016 Emerson Mendes Filho. All rights reserved.
//

import UIKit
import Kingfisher

class RepositoryTableViewCell : UITableViewCell, CellConfigurable {
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var forksCountLabel: UILabel!
    @IBOutlet weak var scoreLabel: UILabel!
    @IBOutlet weak var ownerAvatarImageView: UIImageView!
    @IBOutlet weak var ownerLoginLabel: UILabel!
    @IBOutlet weak var ownerNameLabel: UILabel!
    
    func configureCellWith(viewModelItem: Any) {
        
        let viewModelItem = viewModelItem as! RepositoryViewModel.ViewModelItem
        
        self.nameLabel.text = viewModelItem.name
        self.descriptionLabel.text = viewModelItem.description
        self.forksCountLabel.text = viewModelItem.forksCount
        self.scoreLabel.text = viewModelItem.score
        self.ownerNameLabel.text = "Username"
        self.ownerLoginLabel.text = viewModelItem.ownerLogin
        self.ownerAvatarImageView.kf.setImage(with: viewModelItem.ownerAvatarUrl,
                                              placeholder: Image(named: "no_photo_picture"),
                                              options: [.transition(.fade(1))],
                                              progressBlock: nil,
                                              completionHandler: nil)
    }
}
