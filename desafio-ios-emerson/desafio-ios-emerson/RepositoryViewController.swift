//
//  RepositoryViewController.swift
//  desafio-ios-emerson
//
//  Created by Emerson Mendes Filho on 03/10/16.
//  Copyright © 2016 Emerson Mendes Filho. All rights reserved.
//

import UIKit

class RepositoryViewController: UIViewController, RepositoryDataProviderDelegate {
    
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var tableView: UITableView!
    var refreshControl: UIRefreshControl!
    
    let repositoryViewModel = RepositoryViewModel()
    var repositoryDataProvider = RepositoryDataProvider()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.startLoading()
        self.configureViews()
        self.refresh(sender: self)
    }
    
    func configureViews() {
        
        self.tableView.register(UINib(nibName: String(describing: RepositoryTableViewCell.self), bundle: nil),
                                forCellReuseIdentifier: String(describing: RepositoryTableViewCell.self))
        self.tableView.delegate = self.repositoryDataProvider
        self.tableView.dataSource = self.repositoryDataProvider
        self.tableView.rowHeight = UITableViewAutomaticDimension
        self.tableView.estimatedRowHeight = 115.0
        
        refreshControl = UIRefreshControl()
        refreshControl.attributedTitle = NSAttributedString(string: "Refresh")
        refreshControl.addTarget(self, action: #selector(refresh(sender:)), for: UIControlEvents.valueChanged)
        tableView.addSubview(refreshControl)
        
        self.repositoryDataProvider.addInfiniteScrollCompletion {
            self.repositoryViewModel.loadNextPage(completion: {
                
                self.repositoryDataProvider.isLoadingNextPage = false
                self.tableView.reloadData()
                }, failure: { (error) in
                    
                    self.repositoryDataProvider.isLoadingNextPage = false
                    print(error)
            })
        }
    }
    
    func refresh(sender:AnyObject) {
        
        repositoryViewModel.loadInitalPage(completion: {
            self.stopLoading()
            self.repositoryDataProvider.repositoryViewModel = self.repositoryViewModel
            self.repositoryDataProvider.delegate = self
            self.tableView.reloadData()
        }) { (error) in
            self.stopLoading()
            print(error)
        }
    }
    
    func startLoading() {
        self.activityIndicator.startAnimating()
        self.tableView.isHidden = true
    }
    
    func stopLoading() {
        self.refreshControl.endRefreshing()
        self.activityIndicator.stopAnimating()
        self.tableView.isHidden = false
    }
    
    func selectedRepository(repository: RepositoryModel) {
        performSegue(withIdentifier: "repoToPullSegue", sender: repository)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier == "repoToPullSegue") {
            let vc: PullRequestViewController = segue.destination as! PullRequestViewController
            vc.repository = sender as? RepositoryModel
        }
    }
    
}

