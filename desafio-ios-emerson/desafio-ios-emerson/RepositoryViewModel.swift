//
//  RepositoryViewModel.swift
//  desafio-ios-emerson
//
//  Created by Emerson Mendes Filho on 03/10/16.
//  Copyright © 2016 Emerson Mendes Filho. All rights reserved.
//

import Foundation

class RepositoryViewModel {
    
    var page: Int!
    var repositoryList: [RepositoryModel]?
    
    struct ViewModelItem {
        var name: String
        var description: String
        var ownerLogin: String
        var ownerAvatarUrl: URL
        var score: String
        var forksCount: String
    }
    
    private func loadRepositoryList(completion:@escaping () -> (), failure: @escaping Failure) {
        
        RepositoryModel.loadRepositoryList(page: self.page, success: {[weak self] (repositoryList) in
            
            guard let strongSelf = self else { return }
            
            if (strongSelf.page != 1) {
                strongSelf.repositoryList!.append(contentsOf: repositoryList)
            } else {
                strongSelf.repositoryList = repositoryList
            }
            
            completion()
            
            }) { (errorString) in
                failure(errorString)
        }
    }
    
    func loadInitalPage(completion:@escaping () -> (), failure: @escaping Failure) {
        self.page = 1;
        self.loadRepositoryList(completion: completion, failure: failure)
    }
    
    func loadNextPage(completion:@escaping () -> (), failure: @escaping Failure) {
        self.page = self.page + 1;
        self.loadRepositoryList(completion: completion, failure: failure)
    }
    
    func repositoryForIndex(index: Int) -> RepositoryModel {
        let repositoryModel = self.repositoryList![index]
        return repositoryModel
    }
    
    func itemForIndex(index: Int) -> ViewModelItem {
        
        let repositoryModel = self.repositoryList![index]
        
        let viewModelItem = ViewModelItem(name: repositoryModel.name ?? "",
                                          description: repositoryModel.description ?? "",
                                          ownerLogin: repositoryModel.owner!.login ?? "",
                                          ownerAvatarUrl: (repositoryModel.owner?.avatarUrl)! as URL,
                                          score: String(repositoryModel.score!),
                                          forksCount: String(repositoryModel.forksCount!))
        return viewModelItem
        
        
    }
}
