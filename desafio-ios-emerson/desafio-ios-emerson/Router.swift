//
//  Router.swift
//  desafio-ios-emerson
//
//  Created by Emerson Mendes Filho on 02/10/16.
//  Copyright © 2016 Emerson Mendes Filho. All rights reserved.
//

import Alamofire

enum Router: URLRequestConvertible {
    
    // MARK: Cases
    
    case repositories(page: Int)
    case pullRequests(creator: String, repo: String)
    
    // MARK: - Properties
    
    /// The HTTP method to be used.
    var method : HTTPMethod {
        switch self {
        case .repositories,
             .pullRequests:
            return .get
        }
    }
    
    /// The path to be used for the network request.
    var path : String {
        switch self {
        case .repositories:
            return "search/repositories"
        case .pullRequests(let creator, let repo):
            return "repos/\(creator)/\(repo)/pulls"
        }
    }
    
    /// The URLRequest combining all the required data to execute a network request.
    func asURLRequest() throws -> URLRequest {
        
        let url = try Environment.baseURL!.asURL()
        
        var urlRequest = URLRequest(url: url.appendingPathComponent(path))
        urlRequest.httpMethod = method.rawValue
        
        switch self {
        case .repositories(let page):
            urlRequest = try URLEncoding.default.encode(urlRequest, with: ["q": "language:Java", "sort": "stars", "page": page])
        case .pullRequests:
            urlRequest = try URLEncoding.default.encode(urlRequest, with: nil)
        }
        
        return urlRequest
    }
}
