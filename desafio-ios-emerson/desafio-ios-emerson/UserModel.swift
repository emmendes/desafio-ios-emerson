//
//  UserModel.swift
//  desafio-ios-emerson
//
//  Created by Emerson Mendes Filho on 02/10/16.
//  Copyright © 2016 Emerson Mendes Filho. All rights reserved.
//

import Foundation

import ObjectMapper

class UserModel: Mappable, Equatable {
    
    //
    //MARK: Var Declaration
    //
    
    var login: String?
    var avatarUrl: URL?
    
    //
    //MARK: Init Methods
    //
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        login <- map["login"]
        avatarUrl <- (map["avatar_url"], URLTransform())
    }
}

// MARK: - Equatable Protocol Method

func ==(lhs: UserModel, rhs: UserModel) -> Bool {
    guard let lhsId = lhs.login, let rhsId = rhs.login else {
        return false
    }
    
    return lhsId == rhsId
}
