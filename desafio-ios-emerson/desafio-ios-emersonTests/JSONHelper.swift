//
//  JSONHelper.swift
//  desafio-ios-emerson
//
//  Created by Emerson Mendes Filho on 04/10/16.
//  Copyright © 2016 Emerson Mendes Filho. All rights reserved.
//

import UIKit

class JSONHelper: NSObject {
    static func jsonFromFile(className: AnyClass, name: String) -> [String: AnyObject]? {
        if let masterDataUrl: URL = Bundle(for: className).url(forResource: name, withExtension: "json") {
            
            do {
                let dataFromFile = try Data(contentsOf: masterDataUrl)
                
                guard let dataDictionary = try! JSONSerialization.jsonObject(with: dataFromFile, options: []) as? [String: AnyObject] else {
                    return nil
                }
                
                return dataDictionary
            } catch {
                return nil
            }
        } else {
            return nil
        }
    }
}

