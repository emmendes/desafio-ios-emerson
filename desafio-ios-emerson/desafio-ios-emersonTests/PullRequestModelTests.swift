//
//  PullRequestModelTests.swift
//  desafio-ios-emerson
//
//  Created by Emerson Mendes Filho on 04/10/16.
//  Copyright © 2016 Emerson Mendes Filho. All rights reserved.
//

import XCTest
import ObjectMapper
@testable import desafio_ios_emerson

class PullRequestModelTests: XCTestCase {
    
    var dictionary: [String: AnyObject]!
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
        
        guard let dictionary = JSONHelper.jsonFromFile(className: PullRequestModelTests.self, name:"pull_request") else {
            XCTFail("Could not load data from JSON file.")
            return
        }
        
        self.dictionary = dictionary
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testPullRequestParse_ShouldNotBeNil() {
        
        let object = Mapper<PullRequestModel>().map(JSON: dictionary)
        XCTAssertNotNil(object, "object should not be nil.")
    }
    
    func testPullRequestParse_ShouldMatchValues() {
        
        let object = Mapper<PullRequestModel>().map(JSON: dictionary)

        let expectedTitle = dictionary["title"] as? String
        XCTAssertEqual(object?.title, expectedTitle)
        
        let expectedBody = dictionary["body"] as? String
        XCTAssertEqual(object?.body, expectedBody)
        
        let expectedUser = Mapper<UserModel>().map(JSON: dictionary["user"] as! [String: AnyObject])
        XCTAssertEqual(object?.user, expectedUser)
        
        let expectedCreatedAt = DateTransform().transformFromJSON(dictionary["created_at"])
        XCTAssertEqual(object?.createdAt, expectedCreatedAt)
        
        let expectedHtmlUrl = dictionary["html_url"] as? String
        XCTAssertEqual(object?.htmlUrl?.absoluteString, expectedHtmlUrl)
        
        let expectedState = dictionary["state"] as? String
        XCTAssertEqual(object?.state?.rawValue, expectedState)
    }   
}
