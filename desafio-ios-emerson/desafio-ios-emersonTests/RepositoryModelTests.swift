//
//  RepositoryModelTests.swift
//  desafio-ios-emerson
//
//  Created by Emerson Mendes Filho on 04/10/16.
//  Copyright © 2016 Emerson Mendes Filho. All rights reserved.
//

import XCTest
import ObjectMapper
@testable import desafio_ios_emerson

class RepositoryModelTests: XCTestCase {
    
    var dictionary: [String: AnyObject]!
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
        
        guard let dictionary = JSONHelper.jsonFromFile(className: RepositoryModelTests.self, name:"repository") else {
            XCTFail("Could not load data from JSON file.")
            return
        }
        
        self.dictionary = dictionary
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testRepositoryParse_ShouldNotBeNil() {
        
        let object = Mapper<RepositoryModel>().map(JSON: dictionary)
        XCTAssertNotNil(object, "object should not be nil.")
    }
    
    func testRepositoryParse_ShouldMatchValues() {
        
        let object = Mapper<RepositoryModel>().map(JSON: dictionary)
        
        let expectedName = dictionary["name"] as? String
        XCTAssertEqual(object?.name, expectedName)
        
        let expectedDescription = dictionary["description"] as? String
        XCTAssertEqual(object?.description, expectedDescription)
        
        let expectedOwner = Mapper<UserModel>().map(JSON: dictionary["owner"] as! [String: AnyObject])
        XCTAssertEqual(object?.owner, expectedOwner)
        
        let expectedForksCount = dictionary["forks_count"] as? Int
        XCTAssertEqual(object?.forksCount, expectedForksCount)
        
        let expectedScore = dictionary["score"] as? Int
        XCTAssertEqual(object?.score, expectedScore)
    }
    
}
