//
//  UserModelTests.swift
//  desafio-ios-emerson
//
//  Created by Emerson Mendes Filho on 04/10/16.
//  Copyright © 2016 Emerson Mendes Filho. All rights reserved.
//

import XCTest
import ObjectMapper
@testable import desafio_ios_emerson

class UserModelTests: XCTestCase {
    
    var dictionary: [String: AnyObject]!
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
        
        guard let dictionary = JSONHelper.jsonFromFile(className: UserModelTests.self, name:"user") else {
            XCTFail("Could not load data from JSON file.")
            return
        }
        
        self.dictionary = dictionary
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testUserParse_ShouldNotBeNil() {
        
        let object = Mapper<UserModel>().map(JSON: dictionary)
        XCTAssertNotNil(object, "object should not be nil.")
    }
    
    func testUserParse_ShouldMatchValues() {
        
        let object = Mapper<UserModel>().map(JSON: dictionary)
        
        let expectedLogin = dictionary["login"] as? String
        XCTAssertEqual(object?.login, expectedLogin)
        
        let expectedAvatarUrl = dictionary["avatar_url"] as? String
        XCTAssertEqual(object?.avatarUrl?.absoluteString, expectedAvatarUrl)
    }
}
